
// Global Variables
const wire = orly.hyper.wire;
const bind = orly.hyper.bind;
const q = orly.q;

// hyperApp Module
hyperApp = {
	// Load Menu function - makes an ajax call to the server to get
	// a menu to display on the page
	loadMenu: function loadMenu() {
		bind(q('#menu'))`
			${fetch('menu.htm', {method: 'get'})
			.then(response => response.text())
				.then(text => {
					return wire()`${{html: text}}`;
				})
			}).catch(function(error) {
				console.log('There has been a problem with your fetch operation: ', error.message);
			});
		`;
	} 
}


function resourceTest() { 

	for (var paperUsed=0; paperUsed < 5; paperUsed++) {
		var paperCount = paperUsed;
	}
	
	//console.log("paperUsed=" + paperUsed);
	
	for (stapleJams = 0; stapleJams > -5; stapleJams--) {
		var net = paperCount + stapleJams;
	}
	
	//console.log("stapleJams=" + stapleJams);
	
	//console.log("net=" + net);
}

resourceTest();




